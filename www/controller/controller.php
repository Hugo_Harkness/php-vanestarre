<?php
/**
 * @brief : regroups all the controllers
 */


require_once "lib/Status.php";
require_once "model/Connection.php";
require_once "model/Settings.php";
require_once "model/Message.php";
require_once "model/Profile.php";


function home()
{
    /**
     * @brief : handle everything about the home page
     */
    $setting = new Settings();
    $messages_to_get = intval($setting->load()[2]);
    $status = new Status();
    $messages = array();

    // initialize $user_id
    $user_id = null;
    if (isset($_SESSION["Connection"])) {
        $user_id = $_SESSION["Connection"]->user_id;
    }

    if (isset($_POST["message_id"], $_POST["reaction"])) {        //if posting a reaction
        if (isset($_SESSION["Connection"])) {            // must be connected to continue
            $reaction = $_POST["reaction"];
            $message_id = $_POST["message_id"];

            $current_reaction = Message_model::fetch_user_reaction($message_id, $user_id);
            $modification_success = false;
            $mode = null;

            if ($current_reaction && $current_reaction !== $reaction) { // can’t add an other reaction on the same message
                http_response_code(403);
                die();
            } elseif ($current_reaction && $current_reaction == $reaction)  // if the reaction was already there we delete the reaction (toggle)
                $modification_success = Message_model::remove_reaction($message_id, $user_id);
            else {
                $modification_success = Message_model::add_reaction($message_id, $user_id, $reaction);
                $mode = "add";
            }

            if ($modification_success) {
                $message = Message_model::fetch_message($message_id, $user_id); // get the new message to display it
                if ($mode == "add") {
                    if ($reaction == "Love" && $message->get_nb_love_max() == $message->get_reactions()["Love"] && $message->get_reactions()["Love"] !== 0) { // check for the event where we hit the "max" number of love reactions
                        $notification_text = "";
                        // we got a winner !
                        if (Message_model::disable_nb_love_max($message->get_message_id())) {
                            file_put_contents("liste_donateurs.txt",
                                $_SESSION["Connection"]->username . " (" . $_SESSION["Connection"]->email .
                                ") doit 10 btc ! \n", FILE_APPEND, stream_context_create());
                            $notification_text = "Bravo vous avez été le " . $message->get_reactions()["Love"] . "ème a mettre un <i>Love</i>, vous devez 10 bitcoins à Vanestarre (1B2S4Nf8jD3fshHodzuYhframoQsQaZEcZ) ! ";
                        } else
                            $notification_text = "Vous auriez dû donner 10 bitcoins à Vanestarre mais heureusement le site a buggé ! ";
                        // show the notification
                        require 'view/templateNotification.php';
                    }
                }
                include 'view/templateTiwitt.php';
                die();
            } else {
                http_response_code(500);
                die();
            }

        } else {
            http_response_code(401);
            die();
        }
    }
    // check if the request was made by the JS
    $fetch_mode = false;
    $last_message_id = null;
    if (isset($_POST["message_id"], $_POST["mode"]) && $_POST["mode"] == "fetch") {
        $fetch_mode = true;
        $last_message_id = $_POST["message_id"];
    }

    $messages = false;
    if (isset($_GET["searched_tag"]) && $_GET["searched_tag"] != "") { // filter by tag
        $tag = $_GET["searched_tag"];
        if (substr($tag, 0, 2) === "β") { // remove the beta if the user put it at the start
            $tag = substr($tag, 2);
        }
        $messages = Message_model::fetch_tagged_messages($messages_to_get, $tag, $last_message_id, $user_id);
    } else
        $messages = Message_model::fetch_messages($messages_to_get, $last_message_id, $user_id);

    if ($fetch_mode) {
        if ($messages == false || sizeof($messages) == 0) // send a blank text, will be recognized by the JS
            die();
        include 'view/templateMessageList.php'; // send only the messages, not the entire page
        die();
    } else {
        if ($messages == false || sizeof($messages) == 0) {
            $status = new Status();
            $status->add_event("Aucun message trouvé ...", true);
        }
        require('view/homeView.php'); // send the entire page
    }
}

function login()
{
    if (isset($_SESSION["Connection"])) { // if already connected we redirect to the home page
        header('location:index.php?action=home');
        die();
    }

    $status = new Status();

    if (isset($_POST['username'], $_POST['password'])) { // if we try to connect
        $connection = new Connection();
        if ($connection->login($_POST['username'], $_POST['password'])) { // connection successful, redirect to the home page
            header('location:index.php?action=home');
            die();
        } else {
            $status->add_event("La connexion a échoué", true);
        }
    }
    require('view/loginView.php');
}

function logout()
{
    // juste delete the session
    $connection = new Connection();
    $connection->logout();
    header('location:index.php?action=home');
    die();
}

function signup()
{
    if (isset($_SESSION["Connection"])) { // if already logged in, we redirect to the home page
        header('location:index.php?action=home');
        die();
    }

    $status = new Status();
    if (isset($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password2'])) { // if the for is used
        $connection = new Connection();
        $profile = new Profile();
        if (!$profile->username_exists($_POST["username"])) { // check if the username already exists

            if ($connection->signup($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password2'])) { // sign up successful, redirect to login
                header('location:index.php?action=login');
                die();
            } else {
                $status->add_event("Les mots de passe ne sont pas identiques", true);
            }
        } else {
            $status->add_event("Le nom d’utilisateur existe déjà !", true);

        }
    }
    require('view/signupView.php');

}

function forgottenPasswd()
{
    if (isset($_SESSION["Connection"])) { // that’s useless if we are already connected
        header('location:index.php?action=home');
        die();
    }

    $status = new Status();

    if (isset($_POST['email'])) { // if the form is used wit an email
        $connection = new Connection();
        $connection->genPasswdResetCode($_POST['email']);
        $status->add_event("Si un compte correspond a l'adresse email, un email avec le lien de réinitialisation vous sera envoyer.", false);
    }

    require('view/forgottenPasswdView.php');
}

function resetPasswd()
{
    if (isset($_SESSION["Connection"])) {  // useless if we’re already connected
        header('location:index.php?action=home');
        die();
    }

    if (isset($_GET['code'])) { // if we got a code we try to reset

        $status = new Status();

        if (isset($_POST['password'], $_POST['password2'])) {
            $connection = new Connection();
            if ($connection->resetPasswd($_POST['password'], $_POST['password2'], $_GET['code'])) {
                header('location:index.php?action=login');
                die();
            } else {
                $status->add_event("Les mots de passe ne sont pas identiques", true);
            }
        }

        require('view/resetPasswdView.php');

    } else { // else that’s a broken link, we redirect to the home page
        header('location:index.php?action=home');
        die();
    }
}

function admin()
{
    if (!isset($_SESSION["Connection"]) || $_SESSION["Connection"]->type != "admin") { // you need to be connected with an admin account
        header('location:index.php?action=home');
        die();
    }

    $status = new Status();
    $connection = new Connection();
    $settings = new Settings();

    if (isset($_POST["update_settings"], $_POST["min_love"], $_POST["max_love"], $_POST["nb_load"])) {  // if we used the form to update the settings
        $settings->updateLove($_POST["min_love"], $_POST["max_love"], $_POST["nb_load"], $status);
    }
    if (isset($_GET["user_id"], $_GET["mode"]) && $_GET["mode"] == "delete") { // if we clicked on a "delete" link for an account
        $profile = new Profile($_GET["user_id"]);
        if ($profile->fetch_infos()) {
            if ($profile->get_type() !== "admin") {
                if ($profile->delete($profile->get_user_id())) {
                    header("location:index.php?action=admin"); // reload the page
                    die();
                } else
                    $status->add_event("Erreur pendant la suppression du compte", true);
            } else {
                $status->add_event("Vous ne pouvez pas supprimer un administrateur", true);
            }
        } else {

            $status->add_event("Profile introuvable", true);
        }
    }

    [$min_love, $max_love, $nb_load, $success] = $settings->load(); // get the settings
    if (!$success)
        $status->add_event("Erreur lors des chargements des paramètres", true);

    if (isset($_POST["send_message"], $_POST["text"], $_FILES["images"])) { // if we use the send_message form
        $message = new Message_model();
        $message->send($_POST["text"], $_FILES["images"], $min_love, $max_love, $status);
    }
    $users = $connection->get_users_list();

    require('view/adminView.php');
}

function edit()
{
    if (!isset($_GET['message_id'], $_SESSION["Connection"]) || $_SESSION["Connection"]->type != "admin") { // of course we need to be an admin
        header('location:index.php?action=home');
        die();
    }

    $status = new Status();
    $message = Message_model::fetch_message($_GET['message_id']);

    if (isset($_POST['delete'])) { // if clicked on the delete button
        if (Message_model::delete_message($_GET['message_id'], $status)) {
            header('location:index.php?action=home');
            die();
        }
    }

    if (isset($_POST['text'])) { // if we modified the content of the message
        $delete_img = array();
        if (isset($_POST['delete_img'])) {
            $delete_img = $_POST['delete_img'];
        }
        Message_model::update_message($_POST['text'], $_FILES["images"], $delete_img, $message, $status);
        $message = Message_model::fetch_message($_GET['message_id']); // fetch the message to update it
    }

    require('view/editView.php');
}

function profile()
{
    $status = new Status();

    if (!isset($_GET["user_id"])) { // if no user_id is in the link
        if (isset($_SESSION["Connection"])) { // redirect to the user’s page
            header('location:index.php?action=profile&user_id=' . $_SESSION["Connection"]->user_id);
            die();
        } else {// kick to index.php if not logged in and don’t try to see a profile
            header('location:index.php');
            die();
        }
    }
    $profile = new Profile($_GET["user_id"]);
    $profile->fetch_infos();
    // update infos
    /**
     * check if we have the rights : we need to be the user or an admin
     */
    if (isset($_POST["username"], $_POST["email"]) && ($_SESSION["Connection"]->type == "admin" || $_SESSION["Connection"]->user_id == $_GET["user_id"])) {
        $new_username = $_POST["username"];
        $new_email = $_POST["email"];
        $user_id = $_GET["user_id"];
        if ($new_username == $profile->get_username() || !$profile->username_exists($new_username)) {
            if ($profile->update_infos($user_id, $new_username, $new_email)) {
                $status->add_event("Infos changés avec succès", false);

            } else {
                $status->add_event("Erreur lors de la modification", true);
            }
        } else {
            $status->add_event("Le nom d’utilisateur existe déjà !", true);
        }

    } // only the user itself can change his password
    elseif (isset($_POST["password"], $_POST["new_password"], $_POST["confirm_new_password"]) && $_GET["user_id"] == $_SESSION["Connection"]->user_id) {
        if ($_POST["password"]) {
            $old_password = $_POST["password"];
            $new_password = $_POST["new_password"];
            $confirm_new_password = $_POST["confirm_new_password"];
            if (strlen($new_password) > 0) {
                if ($new_password === $confirm_new_password) {
                    if ($profile->update_password($_GET["user_id"], $old_password, $new_password)) {
                        $status->add_event("Mot de passe mis à jour avec succès", false);
                    } else {
                        $status->add_event("Erreur lors de la modification", true);
                    }

                } else {
                    $status->add_event('Les champs nouveau mot de passe et confirmation du nouveau mot de passe doivent correspondre', true);
                }
            } else {
                $status->add_event('Veuillez insérer un nouveau mot de passe', true);
            }

        } else {
            $status->add_event("Vous devez entrer votre mot de passe pour mettre à jour votre mot de passe", true);
        }
    }

    $result = $profile->fetch_infos();
    if ($result) {
        require 'view/profileView.php';
    } else {
        $status->add_event("Erreur lors de l’affichage du profil, existe-t-il ?", true);
        require 'view/profileView.php';
    }


}

function update_website()
{
    /**
     * @brief little function that must be ran as admin to update the website from the git page
     */
    if (isset($_SESSION["Connection"]) && $_SESSION["Connection"]->type == "admin") {
        $result = shell_exec("git pull");

    }
    header("location:index.php");
}