// get the different elements from the DOM
let menu = document.getElementById("menu");
let button_menu = document.getElementById("button_menu");
let img_menu = document.getElementById("img_menu");



button_menu.addEventListener("click", function () { // if we click on the button
    if (menu.classList.contains("menu_hide")) { // if the menu is closed
        img_menu.src = "public/assets/cross.svg";
        menu.classList.remove("menu_hide");
    } else {
        menu.classList.add("menu_hide");
        img_menu.src = "public/assets/menu.svg";
    }
});
