function add_reaction(message_id, reaction_type) {
    let request = new XMLHttpRequest()
    // bake the request
    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                let message_element = document.getElementById("message" + message_id.toString()) // update the message content
                message_element.outerHTML = this.responseText
            } else {
                console.error(this.responseText)
            }

        }

    }
    request.open("post", "index.php?action=home", true)
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    request.send("message_id=" + message_id.toString() + "&reaction=" + reaction_type)
}

