let loading_indicator = document.getElementById("loading_indicator");
let messages = document.getElementById("messages")
let requests = []
let end_of_messages = false

function is_in_viewport(element) { // check if the element is in the viewport
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function close_notification() { // only used when the client reaches the max of love, close the notification
    let elements = document.getElementsByClassName("important_notification")
    for (const index in elements) {
        elements[index].style.display = "none";
    }
}


function request_messages() {
    if (requests.length < 1) { // check if a request is already sent
        let request = new XMLHttpRequest()
        requests.push(request)
        // bake the request
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    if (this.responseText === "") { // if we receive a blank text, it means that there is nothing more to be loaded
                        loading_indicator.innerText = "Plus de messages à charger"
                        end_of_messages = true
                    } else {
                        messages.innerHTML += this.responseText
                    }
                } else {
                    console.error(this.responseText)
                }
                requests.pop()
            }
        }
        request.open("post", "#", true)
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
        let message_id = messages.lastElementChild.id.slice(7) // get the message id from the element id
        request.send("mode=fetch&message_id=" + message_id.toString())
    }
}

setInterval(() => {
    // check if we need to load more every 100ms
    if (is_in_viewport(loading_indicator) && !end_of_messages) {
        request_messages();
    }
}, 100)
