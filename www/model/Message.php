<?php
require_once 'lib/dbConnect.php';

/*
 * a message, representing a row in the table with everything linked and "fetched"
 */

class Message
{
    public const UPLOADS_FOLDER = "public/uploads/";
    public const REACTIONS_NAMES = array("Love" => "💗", "Swag" => "😎", "Trop stylé" => "👍", "Cute" => "😍");
    private int $message_id = 0;
    private string $text = "";
    private int $nb_love_max = 0;
    private array $images = array();
    private array $reactions = array();
    private ?string $user_reaction = null;

    // fetch every infos in the respecting variables from the informations got from the message row
    public function __construct($pdo, int $message_id, string $text, int $nb_love_max, $image_present, int $user_id = null)
    {
        // base infos
        $this->message_id = $message_id;
        $this->text = $text;
        $this->nb_love_max = $nb_love_max;


        if ($image_present == "1") { // if at least an image is present, load them
            $sql_img = 'select image_id,path from Images where message_id = :message_id';
            $stmt_img = $pdo->prepare($sql_img);
            $stmt_img->bindValue('message_id', $this->message_id, PDO::PARAM_INT);

            $stmt_img->execute(); // throw is propagated in the parent fonction
            $result_img = $stmt_img->fetchAll();
            foreach ($result_img as $item) {
                $this->images[] = $item;
            }
        }
        // get reaction counts
        $sql_reactions = 'select count(reaction) from Reactions where message_id = :message_id and reaction = :reaction';
        $stmt_reactions = $pdo->prepare($sql_reactions);
        $stmt_reactions->bindValue('message_id', $this->message_id, PDO::PARAM_INT);
        foreach (self::REACTIONS_NAMES as $name => $item) {
            $stmt_reactions->bindValue('reaction', $name);
            $stmt_reactions->execute();
            $result_reactions = $stmt_reactions->fetch();
            $this->reactions[$name] = intval($result_reactions[0]);

        }

        // if the user id is provided, check which reaction he selected
        if ($user_id) {
            $this->user_reaction = Message_model::fetch_user_reaction($this->message_id, $user_id);
        }
    }

    /**
     * @return int
     */
    public function get_message_id(): int
    {
        return $this->message_id;
    }

    /**
     * @return string
     */
    public function get_text(): string
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function get_nb_love_max(): int
    {
        return $this->nb_love_max;
    }

    /**
     * @return array
     */
    public function get_images(): array
    {
        return $this->images;
    }

    /**
     * @return array
     */
    public function get_reactions(): array
    {
        return $this->reactions;
    }

    /**
     * @return ?string
     */
    public function get_user_reaction(): ?string
    {
        return $this->user_reaction;
    }

}

/**
 * Class Message_model handling everything concerning the model, factorized by using the Message class
 */
class Message_model
{

    /**
     * @brief send a message
     * @param $text : the text message
     * @param $images : the images from the $_FILE variable
     * @param $min_love : to generate the random "nb_max_love" value
     * @param $max_love : to generate the random "nb_max_love" value
     * @param $status : to inform of events
     */
    public function send($text, $images, $min_love, $max_love, $status)
    {
        // if the char limit is exceeded
        if (strlen($text) > 50) {
            $status->add_event("Vous avez dépassé la limite de caractères dans un message", true);
            return;
        }
        // quick way to know if there is images
        $images_present = $images["name"][0] != "";

        // parse all the tags from the text
        preg_match_all('/β\w+/', $text, $tags);

        // prepare the message insertion
        $pdo = dbConnect();
        $sql = 'INSERT INTO Messages (text, image_present, nb_love_max)
                VALUE (:text, :image_present, :nb_love_max)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('text', $text, PDO::PARAM_STR);
        $stmt->bindValue('image_present', $images_present, PDO::PARAM_BOOL);
        $nb_max_love = rand($min_love, $max_love);
        $stmt->bindValue('nb_love_max', $nb_max_love, PDO::PARAM_INT);

        // prepare the tags insertion
        $sql_tags = 'INSERT INTO Tags (message_id, tag) VALUE (:message_id, :tag)';
        $stmt_tags = $pdo->prepare($sql_tags);

        // prepare the images insertion
        $sql_image = 'INSERT INTO Images (message_id, path) VALUE (:message_id, :path)';
        $stmt_image = $pdo->prepare($sql_image);

        // checkpoint, we need to go back if one command fails
        $pdo->beginTransaction();
        try {
            // insert the message
            $stmt->execute();
            $message_id = $pdo->lastInsertId(); // get the id
            // insert all the tags
            foreach ($tags[0] as $tag) {
                $stmt_tags->bindParam('message_id', $message_id);
                $substr = substr($tag, 2);
                $stmt_tags->bindParam('tag', $substr);
                $stmt_tags->execute();
            }

            if ($images_present) { // insert all the images if needed
                foreach ($images["name"] as $index => $image) {
                    if ($images["error"][$index] != 0) { // stop if there is an error, we might run out of disk space
                        $status->add_event("Erreur lors de l’upload, code d’erreur : " . $images["error"][$index], true);
                        throw new Exception();
                    }
                    $out_path = Message::UPLOADS_FOLDER . uniqid() . $images["name"][$index]; // generate a name
                    move_uploaded_file($images["tmp_name"][$index], $out_path);
                    $stmt_image->bindParam('message_id', $message_id);
                    $stmt_image->bindParam('path', $out_path);
                    $stmt_image->execute();
                }
            }

            $pdo->commit(); // everything succeeded, we save
            $status->add_event("Message envoyé", false);
        } catch (Exception $e) {
            // uh oh, an error occurred
            $pdo->rollBack();
            $status->add_event("Erreur lors de l’envoi", true);
        }
    }

    /**
     * @brief gets $nb_message
     * @param int $nb_message
     * @param int|null $before_id : get the message before this id
     * @param string|null $user_id : used for the reactions
     * @return array|false
     */
    public static function fetch_messages(int $nb_message, int $before_id = null, string $user_id = null)
    {
        $pdo = dbConnect();
        $stmt = null;

        if ($before_id) { // if we need message before this id
            $sql = 'SELECT message_id, text, image_present, nb_love_max
            from Messages
            where Messages.message_id < :before_id
            order by Messages.message_id DESC 
            LIMIT :number; ';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('before_id', $before_id, PDO::PARAM_INT);
        } else { // we just get the last messages
            $sql = 'SELECT message_id, text, image_present, nb_love_max
            from Messages
            order by Messages.message_id DESC 
            LIMIT :number; ';
            $stmt = $pdo->prepare($sql);
        }
        $stmt->bindValue('number', $nb_message, PDO::PARAM_INT);
        try {
            // execute the queries
            $stmt->execute();
            if ($stmt->rowCount() >= 1) {
                $messages = array();
                $results = $stmt->fetchAll(PDO::FETCH_OBJ);
                // initialize every messages

                foreach ($results as $result) {
                    $message = new Message($pdo, $result->message_id,
                        $result->text, $result->nb_love_max, $result->image_present, $user_id);
                    $messages[] = $message;
                }
                return $messages;
            }
        } catch (Exception $exception) {
            // in case of failure we return false
            return false;
        }
        return false;
    }

    /**
     * @brief same as fetch_messages but with tag search
     * @param int $nb_message
     * @param int|null $before_id : get the message before this id
     * @param string|null $user_id : used for the reactions
     * @param string $tag : the tag to search
     * @return array|false
     */
    public static function fetch_tagged_messages(int $nb_message, string $tag, int $before_id = null, string $user_id = null)
    {
        $pdo = dbConnect();

        $stmt = null;
        if ($before_id) {// if we need message before this id
            $sql = 'SELECT message_id, text, image_present, nb_love_max
            from Messages
            where Messages.message_id < :before_id
              AND Messages.message_id IN (SELECT Tags.message_id FROM Tags WHERE Tags.tag = :tag)
            order by Messages.message_id DESC 
            LIMIT :number; ';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('before_id', $before_id, PDO::PARAM_INT);
        } else {// we just get the last messages
            $sql = 'SELECT message_id, text, image_present, nb_love_max
            from Messages
            where Messages.message_id IN (SELECT Tags.message_id FROM Tags WHERE Tags.tag = :tag)
            order by Messages.message_id DESC 
            LIMIT :number; ';
            $stmt = $pdo->prepare($sql);
        }
        $stmt->bindValue('number', $nb_message, PDO::PARAM_INT);
        // bind the tag
        $stmt->bindValue('tag', $tag, PDO::PARAM_STR);
        try {// execute the queries
            $stmt->execute();
            if ($stmt->rowCount() >= 1) {
                $messages = array();
                $results = $stmt->fetchAll();
                // initialize every messages
                foreach ($results as $result) {
                    $message = new Message($pdo, $result["message_id"],
                        $result["text"], $result["nb_love_max"], $result["image_present"], $user_id);
                    $messages[] = $message;
                }
                return $messages;
            }
        } catch (Exception $exception) {
            return false;
        }
        return false;
    }

    /**
     * @brief fetch one message with $message_id as id
     * @param int $message_id
     * @param int|null $user_id
     * @return false|Message
     */
    public static function fetch_message(int $message_id, int $user_id = null)
    {
        $pdo = dbConnect();
        $stmt = null;
        //prepare the query
        $sql = 'SELECT message_id, text, image_present, nb_love_max
            from Messages
            where Messages.message_id = :message_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('message_id', $message_id, PDO::PARAM_INT);


        try {
            // execute the query
            $stmt->execute();
            if ($stmt->rowCount() >= 1) {
                $result = $stmt->fetch();
                // return the built message
                return new Message($pdo, $result["message_id"],
                    $result["text"], $result["nb_love_max"], $result["image_present"], $user_id);
            }
        } catch (Exception $exception) {
            return false;

        }
        return false;
    }

    /**
     * @brief update the message designated by $message
     * @param $text
     * @param $images : vient de $_FILES
     * @param $del_images
     * @param Message $message
     * @param $status
     */
    public static function update_message($text, $images, $del_images, Message $message, $status)
    {
        // get the message id
        $message_id = $message->get_message_id();
        // get the number of changes
        $nb_old_images = count($message->get_images());
        $nb_del_images = count($del_images);

        if (strlen($text) > 50) {
            $status->add_event("Vous avez dépassé la limite de caractères dans un message", true);
            return;
        }

        // if images remains from the old message
        $old_images_present = $nb_old_images > $nb_del_images;
        // check if there is new images
        $new_images_present = $images["name"][0] != "";
        $images_present = $old_images_present || $new_images_present;

        // get the tags
        preg_match_all('/β\w+/', $text, $tags);

        $pdo = dbConnect();

        // prepare the message update query
        $sql = 'UPDATE Messages set text=:text, image_present=:image_present WHERE message_id=:message_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam('text', $text, PDO::PARAM_STR);
        $stmt->bindValue('image_present', $images_present, PDO::PARAM_BOOL);
        $stmt->bindParam('message_id', $message_id, PDO::PARAM_INT);

        // prepare the query to delete all tags
        $sql_delete_tags = 'DELETE FROM Tags WHERE message_id=:message_id';
        $stmt_delete_tags = $pdo->prepare($sql_delete_tags);
        $stmt_delete_tags->bindParam('message_id', $message_id, PDO::PARAM_INT);

        // prepare the query to add all tags
        $sql_tags = 'INSERT INTO Tags (message_id, tag) VALUE (:message_id, :tag)';
        $stmt_tags = $pdo->prepare($sql_tags);

        // prepare the query to delete the images
        $sql_delete_image = 'DELETE FROM Images WHERE image_id=:image_id';
        $stmt_delete_image = $pdo->prepare($sql_delete_image);

        // prepare the query to add images
        $sql_image = 'INSERT INTO Images (message_id, path) VALUE (:message_id, :path)';
        $stmt_image = $pdo->prepare($sql_image);

        try {
            // checkpoint here
            $pdo->beginTransaction();
            // modify the message
            $stmt->execute();
            //delete all tags
            $stmt_delete_tags->execute();

            // add the tags
            foreach ($tags[0] as $tag) {
                $stmt_tags->bindParam('message_id', $message_id);
                $substr = substr($tag, 2); // apparently the β count as 2 chars
                $stmt_tags->bindParam('tag', $substr);
                $stmt_tags->execute();
            }
            //delete the images
            foreach ($del_images as $del_image) {
                $stmt_delete_image->bindParam('image_id', $del_image, PDO::PARAM_INT);
                $stmt_delete_image->execute();
            }
            // add the images
            if ($new_images_present) {

                foreach ($images["name"] as $index => $image) {
                    if ($images["error"][$index] != 0) {
                        $status->add_event("Erreur lors de l’upload, code d’erreur : " . $images["error"][$index], true);
                        throw new Exception();
                    }
                    $out_path = Message::UPLOADS_FOLDER . uniqid() . $images["name"][$index];
                    move_uploaded_file($images["tmp_name"][$index], $out_path);
                    $stmt_image->bindParam('message_id', $message_id);
                    $stmt_image->bindParam('path', $out_path);
                    $stmt_image->execute();
                }
            }

            // everything succeeded, save
            $pdo->commit();
            $status->add_event("Message modifié", false);
        } catch (Exception $e) {
            $pdo->rollBack();
            $status->add_event("Erreur lors de la modification", true);
        }
    }

    /**
     * @brief delete the message with id $message_id
     * @param int $message_id
     * @param $status : to pass the events
     * @return bool
     */
    public static function delete_message(int $message_id, $status)
    {
        $pdo = dbConnect();

        //prepare the query to delete the tags
        $sql_delete_tags = 'DELETE FROM Tags WHERE message_id=:message_id';
        $stmt_delete_tags = $pdo->prepare($sql_delete_tags);
        $stmt_delete_tags->bindParam('message_id', $message_id, PDO::PARAM_INT);

        //prepare the query to delete the images
        $sql_delete_images = 'DELETE FROM Images WHERE message_id=:message_id';
        $stmt_delete_images = $pdo->prepare($sql_delete_images);
        $stmt_delete_images->bindParam('message_id', $message_id, PDO::PARAM_INT);

        //prepare the query to delete the reactions
        $sql_delete_reactions = 'DELETE FROM Reactions WHERE message_id=:message_id';
        $stmt_delete_reactions = $pdo->prepare($sql_delete_reactions);
        $stmt_delete_reactions->bindParam('message_id', $message_id, PDO::PARAM_INT);


        //prepare the query to delete the message
        $sql = 'DELETE FROM Messages WHERE message_id=:message_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam('message_id', $message_id, PDO::PARAM_INT);

        try {
            // checkpoint
            $pdo->beginTransaction();
            //delete tags
            $stmt_delete_tags->execute();
            // delete images
            $stmt_delete_images->execute();
            // delete reactions
            $stmt_delete_reactions->execute();
            // delete the message
            $stmt->execute();
            $pdo->commit();
            return true;
        } catch (PDOException $e) {
            $pdo->rollBack();
            $status->add_event("Erreur lors de la suppression", true);
            return false;
        }
    }

    /**
     * @brief gets the name of the reaction the user put on the message, null if none
     * @param int $message_id : the message id
     * @param int $user_id : the user id
     * @return mixed|null
     */
    public static function fetch_user_reaction(int $message_id, int $user_id)
    {
        $pdo = dbConnect();
        // prepare the query
        $sql = 'select reaction from Reactions 
                            where message_id = :message_id and user_id = :user_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('message_id', $message_id, PDO::PARAM_INT);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        try {
            // get the reaction
            $stmt->execute();
            if ($stmt->rowCount() == 1) { // there should be 0 or 1 reaction
                return $stmt->fetch()[0]; // return the reaction
            } else {
                return null; // there’s no reaction
            }
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * @brief adds a reaction to a message
     * @param int $message_id : the message id
     * @param string $user_id : the user id
     * @param string $reaction : the name of the reaction
     * @return bool : success of the operation
     */
    public static function add_reaction(int $message_id, string $user_id, string $reaction): bool
    {
        // check if we hav a valid reaction name
        $reaction_valid = false;
        foreach (Message::REACTIONS_NAMES as $NAME => $item) {
            if ($NAME == $reaction) $reaction_valid = true;
        }
        if (!$reaction_valid) return false;

        $pdo = dbConnect();
        // prepare the query
        $sql = 'insert into Reactions (user_id, message_id, reaction)  value (:user_id , :message_id, :reaction)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('message_id', $message_id, PDO::PARAM_INT);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindValue('reaction', $reaction);
        try {
            // execute the query
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                return true;
            }
        } catch (PDOException $exception) {
            return false;
        }
        return false;
    }

    /**
     * @brief sets the nb_love_max to 0 so no other users gets the notification
     * @param int $message_id : the message id
     * @return bool : true if succeeded
     */
    public static function disable_nb_love_max(int $message_id): bool
    {
        $pdo = dbConnect();
        // prepare the query
        $sql = 'update Messages set nb_love_max = 0 where message_id = :message_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('message_id', $message_id, PDO::PARAM_INT);
        try {
            //execute the query, single operation, don’t need to do a transaction
            $stmt->execute();
            if ($stmt->rowCount() == 1) return true;
        } catch (PDOException $exception) {
            return false;
        }
        return false;
    }

    /**
     * @brief removes the reaction of the user on the message
     * @param int $message_id : the message id
     * @param string $user_id : the user id
     * @return bool : returns true on success
     */
    public static function remove_reaction(int $message_id, string $user_id): bool
    {
        $pdo = dbConnect();
        // initialize the query
        $sql = 'delete from Reactions where user_id = :user_id and message_id = :message_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('message_id', $message_id, PDO::PARAM_INT);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        try {
            //execute the query, single operation, don’t need to do a transaction
            $stmt->execute();
        } catch (PDOException $exception) {
            return false;
        }
        return true;


    }

}