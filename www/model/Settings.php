<?php
require_once 'lib/dbConnect.php';

class Settings
{
    public function load()
    {
        /**
         * @brief: return the configs value or false
         */
        $pdo = dbConnect();
        $sql = 'SELECT setting_value FROM Settings WHERE setting_id = :setting_id';
        $stmt_min_love = $pdo->prepare($sql);
        $stmt_nb_load = $pdo->prepare($sql);
        $stmt_max_love = $pdo->prepare($sql);
        $stmt_min_love->bindValue('setting_id', 'min_love', PDO::PARAM_STR);
        $stmt_max_love->bindValue('setting_id', 'max_love', PDO::PARAM_STR);
        $stmt_nb_load->bindValue('setting_id', 'messages_to_load', PDO::PARAM_STR);

        try {
            $stmt_min_love->execute();
            $stmt_max_love->execute();
            $stmt_nb_load->execute();
            if ($stmt_min_love->rowCount() == 1 && $stmt_max_love->rowCount() == 1) {
                $stmt_min_love->setFetchMode(PDO::FETCH_OBJ);
                $stmt_max_love->setFetchMode(PDO::FETCH_OBJ);
                $stmt_nb_load->setFetchMode(PDO::FETCH_OBJ);
                $min_love = $stmt_min_love->fetch()->setting_value;
                $max_love = $stmt_max_love->fetch()->setting_value;
                $nb_load = $stmt_nb_load->fetch()->setting_value;
                return array($min_love, $max_love, $nb_load, true);
            } else {
                return array(0, 0, 0, false);
            }
        } catch (PDOException $e) {
            return array(0, 0, 0, false);
        }
    }

    public function updateLove($min_love, $max_love, $nb_load, $status)
    {
        /**
         * @brief: set the configs value
         */
        if ($min_love > 0 and $max_love > 0) { // min and max love must be positive
            if ($min_love <= $max_love) { // <= so we can force the randomness
                if ($nb_load < 1) {
                    $status->add_event("Nombre de messages à charger incorrect (doit être >=1)", true);
                    return;
                }
                $pdo = dbConnect();

                $sql_change_love = 'UPDATE Settings
                                    SET setting_value = :setting_value
                                    WHERE setting_id = :setting_id ';
                $stmt_max = $pdo->prepare($sql_change_love);
                $stmt_max->bindValue('setting_value', $max_love, PDO::PARAM_STR);
                $stmt_max->bindValue('setting_id', "max_love", PDO::PARAM_STR);

                $stmt_min = $pdo->prepare($sql_change_love);
                $stmt_min->bindValue('setting_value', $min_love, PDO::PARAM_STR);
                $stmt_min->bindValue('setting_id', "min_love", PDO::PARAM_STR);
                $stmt_nb_load = $pdo->prepare($sql_change_love);
                $stmt_nb_load->bindValue('setting_value', $nb_load, PDO::PARAM_STR);
                $stmt_nb_load->bindValue('setting_id', "messages_to_load", PDO::PARAM_STR);

                $pdo->beginTransaction();
                try {
                    $stmt_max->execute();
                    $stmt_min->execute();
                    $stmt_nb_load->execute();
                    $pdo->commit();
                    $status->add_event("Modifications effectuées avec succès", false);
                } catch (Exception $e) {
                    $pdo->rollBack();
                    $status->add_event("Erreur lors de la modification", true);
                }

            } else {
                $status->add_event("Min doit être inférieur à max", true);
            }
        } else {
            $status->add_event("Valeurs incorrectes", true);
        }
    }
}