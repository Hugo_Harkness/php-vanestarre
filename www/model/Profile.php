<?php
require_once 'lib/dbConnect.php';

/**
 * Class Profile, represents a profile with all the informations
 */
class Profile
{
    private ?int $user_id;
    private string $username = "";
    private string $email = "";
    private string $type = "";

    public function __construct(int $user_id = null)
    {
        $this->user_id = $user_id;
    }


    /**
     * @return string
     */
    public function get_username(): string
    {
        return $this->username;
    }


    /**
     * @return string
     */
    public function get_user_id(): string
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function get_email(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function get_type(): string
    {
        return $this->type;
    }

    /**
     * @brief gets the infos of the user and stores it on the database
     * @param int|null $user_id : the user’s id , if null the attribute $this->user_id is used
     * @return bool : returns true on success
     */
    public function fetch_infos(int $user_id = null)
    {
        // if the user id is not in parameter, use the attribute
        if (!$user_id) {
            $user_id = $this->user_id;
        }
        // we need the user id
        if (!$user_id) return false;
        $pdo = dbConnect();
        //initialize the query
        $sql = 'SELECT email,username, type FROM Users WHERE user_id = :user_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        try {
            // execute the query
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                // get the result in the attributes
                $stmt->setFetchMode(PDO::FETCH_OBJ);
                $result = $stmt->fetch();
                $this->email = $result->email;
                $this->username = $result->username;
                $this->type = $result->type;
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @brief  updates the user name and email
     * @param int $user_id : the user id
     * @param string $new_username : the username to write
     * @param string $new_email : the email to be written
     * @return bool : returns true on success
     */
    public function update_infos(int $user_id, string $new_username, string $new_email): bool
    {
        $pdo = dbConnect();
        // set up the query
        $sql = 'UPDATE Users SET username = :new_username, email = :email
                                    WHERE user_id = :user_id;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('new_username', $new_username, PDO::PARAM_STR);
        $stmt->bindValue('email', $new_email, PDO::PARAM_STR);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        try {
            // execute the query
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                // update the attributes
                $this->username = $new_username;
                $this->email = $new_email;
                return true;
            }
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @brief changes the password of the account
     * @param int $user_id : the user id
     * @param string $old_password : the old password for verification
     * @param string $new_password : the new password
     * @return bool : returns true on success
     */
    public function update_password(int $user_id, string $old_password, string $new_password): bool
    {
        $pdo = dbConnect();
        // set up the query
        $sql = 'UPDATE Users SET passwd= SHA1(:new_password)
                                    WHERE user_id = :user_id and passwd = SHA1(:old_password)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindValue('new_password', $new_password, PDO::PARAM_STR);
        $stmt->bindValue('old_password', $old_password, PDO::PARAM_STR);
        try {
            //execute the query
            $stmt->execute();
            if ($stmt->rowCount() == 1) { // check if it worked
                return true;
            }
        } catch (PDOException $e) {
            return false;
        }
        return false;
    }

    /**
     * @brief deletes the user
     * @param int $user_id : the user id
     * @return bool : returns true on success
     */
    public function delete(int $user_id): bool
    {
        $pdo = dbConnect();
        // setup the query to delete the reactions
        $sql_reactions = 'delete from Reactions where user_id = :user_id';
        $stmt_reactions = $pdo->prepare($sql_reactions);
        $stmt_reactions->bindValue("user_id", $user_id);
        // setup the query to delete the user
        $sql = 'delete from Users where user_id = :user_id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue("user_id", $user_id);

        try {
            // multiple changes, we need to do a transaction
            $pdo->beginTransaction();
            // delete the reactions
            $stmt_reactions->execute();
            // delete the user
            $stmt->execute();
            // save the changes
            $pdo->commit();
            return true;
        } catch (PDOException $exception) {
            $pdo->rollBack();
            return false;
        }
    }

    /**
     * @brief check if the username is in the database
     * @param string $username : the username wanted
     * @return bool : returns true if the username exists or an error occured
     */
    function username_exists(string $username): bool
    {
        $pdo = dbConnect();
        //set up the query
        $sql = 'select username from Users where username = :username';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue("username", $username);
        try {
            //execute the query
            $stmt->execute();
            if ($stmt->rowCount() == 0) { // 0 rows means no users having this username
                return false;
            }
        } catch (PDOException$exception) {
            return true;
        }
        return true;
    }

}