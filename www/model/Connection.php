<?php
require_once 'lib/dbConnect.php';

class Connection
{
    public function login(string $username, $passwd)
    {
        /**
         * @brief: check if the combination username + password exist in the db
         *          AND load the user information into $_SESSION['Connection']
         */

        $pdo = dbConnect();
        $sql = 'SELECT user_id,username, email, type FROM Users WHERE username = :username AND passwd = SHA1(:passwd)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('username', $username, PDO::PARAM_STR);
        $stmt->bindValue('passwd', $passwd, PDO::PARAM_STR);

        try {
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                $stmt->setFetchMode(PDO::FETCH_OBJ);
                $result = $stmt->fetch();
                $_SESSION['Connection'] = $result;
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    public function logout()
    {
        /**
         * @brief: remove the user information form $_SESSION['Connection']
         *           effectively disconnecting him.
         */
        unset ($_SESSION['Connection']);
    }

    public function signup($username, $email, $passwd, $passwd2)
    {
        if ($passwd == $passwd2) { //check password confirmation

            $pdo = dbConnect();
            //add user in the db
            $sql = 'INSERT INTO Users (username, email, passwd, type) value ( :username, :email, SHA1(:passwd), \'member\')';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('username', $username, PDO::PARAM_STR);
            $stmt->bindValue('email', $email, PDO::PARAM_STR);
            $stmt->bindValue('passwd', $passwd, PDO::PARAM_STR);

            try {
                $stmt->execute();
                if ($stmt->rowCount() == 1) {
                    return true;
                }
            } catch (PDOException $e) {
                return false;
            }
        } else
            return false;
    }

    public function genPasswdResetCode($email)
    {
        /**
         * @brief: add password reset code to the database
         */
        $pdo = dbConnect();
        $sql = 'UPDATE Users SET pass_reset_code = :reset_code WHERE email = :email';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('email', $email, PDO::PARAM_STR);
        $reset_code = uniqid(); // generate uniq reset code
        $stmt->bindValue('reset_code', $reset_code, PDO::PARAM_STR);

        try {
            $stmt->execute();
            if ($stmt->rowCount() == 1) {
                $msg = 'Follow the link to reset your password: https://' . $_SERVER['HTTP_HOST'] . '/index.php?action=resetPasswd&code=' . $reset_code;
                if (mail($_POST['email'], 'Password reset', $msg)) //send email containing link to the reset page with reset code
                    return true;
                else
                    return false;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    public function resetPasswd($passwd, $passwd2, $code)
    {
        /**
         * @brief: change password and remove reset code
         */
        if ($passwd == $passwd2) {//check password confirmation

            $pdo = dbConnect();
            $sql = 'UPDATE Users SET passwd = sha1(:passwd), pass_reset_code = null WHERE pass_reset_code = :code';
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('passwd', $passwd, PDO::PARAM_STR);
            $stmt->bindValue('code', $code, PDO::PARAM_STR);

            try {
                $stmt->execute();
                if ($stmt->rowCount() == 1) {
                    return true;
                }
            } catch (PDOException $e) {
                return false;
            }
        } else
            return false;
    }

    public function get_users_list(): ?array
    {
        /**
         * @brief: get list of all user
         */
        $pdo = dbConnect();
        $sql = 'select user_id,username,type from Users';
        $stmt = $pdo->prepare($sql);
        try {
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $exception) {
            return null;
        }


    }
}
