<?php
/**
 * @brief : this files serves as a router to the different controller functions
 */
session_start();

require('controller/controller.php');

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'home') {
        home();
    } elseif ($_GET['action'] == 'login') {
        login();
    } elseif ($_GET['action'] == 'logout') {
        logout();
    } elseif ($_GET['action'] == 'signup') {
        signup();
    } elseif ($_GET['action'] == 'forgottenPasswd') {
        forgottenPasswd();
    } elseif ($_GET['action'] == 'resetPasswd') {
        resetPasswd();
    } elseif ($_GET['action'] == 'admin') {
        admin();
    } elseif ($_GET['action'] == 'edit') {
        edit();
    } elseif ($_GET['action'] == 'profile') {
        profile();
    } elseif ($_GET['action'] == 'update') {
        update_website();
    }
} else {
    // go to the home page by default
    home();
}
