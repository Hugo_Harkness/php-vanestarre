<?php
function dbConnect()
{
    //get database credential from the environement
    $dbUsername = getenv('DATABASE_USERNAME');
    $dbPassword = getenv('DATABASE_PASSWORD');
    $dbName = getenv('DATABASE_NAME');
    $dbHost = getenv('DATABASE_HOST');

    //or from 'dbCred.inc.php'
    if (!$dbUsername || !$dbPassword || !$dbName || !$dbHost)
        include 'dbCred.inc.php';

    //connect to the database
    try {
        $dsn = 'mysql:host=' . $dbHost . ';dbname=' . $dbName . ';charset=utf8';
        $pdo = new PDO($dsn, $dbUsername, $dbPassword);
    } catch (PDOException $e) {
        die('Erreur : ' . $e->getMessage());
    }
    return $pdo;
}