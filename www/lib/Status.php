<?php

/**
 * Class Status_event used only in Status
 */
class Status_event
{

    private string $message;
    private bool $error; // "info" or "error"

    public function __construct(string $message, bool $is_error)
    {
        $this->message = $message;
        $this->error = $is_error;
    }

    public function show()
    {

        $classes = "event";
        if ($this->error) { // add the error class so it shows in red
            $classes .= " error";
        }
        // output the html
        echo "<div class='$classes'>
               " . $this->message . "</div>";
    }

}

/**
 * Class Status handling the display of multiple errors/infos/events
 */
class Status
{
    /**
     * @var array<Status_event>
     */
    private array $messages;

    public function __construct()
    {
        $this->messages = array();
    }

    public function add_event(string $message, bool $is_error = false)
    {
        // add an event, simple
        $this->messages[] = new Status_event($message, $is_error);
    }

    public function show()
    {
        //show all the messages, ordered as it comes
        foreach ($this->messages as $message) {
            $message->show();
        }
    }

}