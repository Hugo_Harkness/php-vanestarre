<div class="tiwit" id="message<?= $message->get_message_id() ?>">

    <p class="text_tiwit"> <?= preg_replace('/β\w+/', '<strong>$0</strong>', $message->get_text()) ?></p>
    <?php if (sizeof($message->get_images()) > 0) : ?>
        <div class="msg_images">
            <?php foreach ($message->get_images() as $image) : ?>
                <a href="<?= $image["path"] ?>"><img src="<?= $image["path"] ?>" alt="vanestarre’s image"></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="buttons">
        <?php foreach ($message::REACTIONS_NAMES as $name => $emoji) : ?>
            <span class="clickable button reaction_button "
                  onclick="add_reaction(<?= $message->get_message_id() ?>,'<?= $name ?>');"
                  class="button reaction_button <?php if ($message->get_user_reaction() == $name) echo 'reaction_selected' ?>"
                  title="<?= $name ?>"><?= $emoji ?><span
                        class="count_love"><?= $message->get_reactions()[$name] ?></span></span>
        <?php endforeach; ?>
        <?php if (isset($_SESSION['Connection']) && $_SESSION["Connection"]->type == "admin") : ?>
            <a href="index.php?action=edit&message_id=<?= $message->get_message_id() ?>" class="button">Modifier</a>
        <?php endif; ?>
    </div>
    <?php if (!isset($_SESSION["Connection"])) : ?>
        <p class="signup_request">vous devez avoir un compte pour réagir aux messages : <a
                    href="index.php?action=signup">inscription</a>
        </p>
    <?php endif; ?>
</div>