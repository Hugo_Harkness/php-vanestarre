<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VANESTARRE</title>
    <link rel="stylesheet" href="public/css/style.css">
</head>
<body>
<!--pour remonter tout en haut-->
<div id="top"></div>

<header class="header">
    <div class="nav_bar">
        <a href="#top" id="button_top"><img src="public/assets/v_for_vanestarre.svg" alt="vers le haut"></a>
        <div><a href="index.php?action=home#top"> VANESTARRE </a></div>
        <div id="button_menu"><img id="img_menu" src="public/assets/menu.svg" alt="menu"></div>
        <div id="menu" class="menu_hide">
            <?php if (isset($_SESSION['Connection'])) : ?>
                <?php if ($_SESSION["Connection"]->type == "admin") : ?>
                    <a href="index.php?action=admin" class="h_link1">Admin</a>
                <?php endif; ?>
                <a href="index.php?action=logout" class="h_link2">Déconnexion</a>
                <a href="index.php?action=profile" class="h_link3">Profil</a>
            <?php else: ?>
                <a href="index.php?action=login" class="h_link2">Connexion</a><br>
                <a href="index.php?action=signup" class="h_link3">Inscription</a>
            <?php endif; ?>
        </div>
    </div>
</header>

<div class="content">
    <div id="status">
        <?php $status->show(); ?>
    </div>
    <?= $content ?>

</div>

<!--menu needs to be loaded before this script-->
<script src="public/js/menu.js"></script>
</body>
</html>