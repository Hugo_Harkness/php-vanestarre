<?php ob_start(); ?>
    <div class="form_box">

        <h2 id="edit_message">
            modifier un message</h2>
        <form action="#" method="post" enctype="multipart/form-data">
            <label>
                contenu
                <br/>
                <textarea name="text" placeholder="50 caractères max"
                          class="input_txt"><?= $message->get_text() ?></textarea>
            </label>
            <br/>
            <h3>Sélectionner des images à supprimer</h3>
            <div class="edit_images_list">

                <?php foreach ($message->get_images() as $image): ?>
                    <label>
                        <input type="checkbox" name="delete_img[]" value="<?= $image["image_id"] ?>"><img
                                src="<?= $image["path"] ?>">
                    </label>
                <?php endforeach; ?>
            </div>
            <div>

                <label>
                    insérer des images <br/>
                    <input type="file" accept="image/*,.jpg,.jpeg,.gif,.png,.bmp" name="images[]" multiple>

                </label>
            </div>
            <input type="submit" value="modifier" class="button submit">
        </form>
        <h3>Attention !</h3>
        <form action="#" method="post">
            <input type="submit" name="delete" class="button submit" value="Supprimer le Message !">
        </form>
    </div>


<?php
$content = ob_get_clean();
require('template.php');
?>