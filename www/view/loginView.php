<?php ob_start(); ?>

<div class="form_box">

    <form method="post" action="#">
        <label for="username">Identifiant :</br></label>
        <input type="text" name="username" id="username" class="input_txt"
               value="<?php if (isset($_POST['username'])) {
                   echo $_POST['username'];
               } ?>" required/><br/>
        <label for="password">Mot de passe :</br></label>
        <input type="password" name="password" id="password" class="input_txt"
               required/>
        <input type="submit" name="Login" id="submit" value="Connexion" class="button submit"/>
    </form>

    <a href="index.php?action=forgottenPasswd">Mot de passe oublié ?</a>
</div>


<?php
$content = ob_get_clean();
require('template.php');
?>
