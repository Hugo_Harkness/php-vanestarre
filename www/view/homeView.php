<?php ob_start(); ?>
<h1>Voici les messages de Vanestarre : </h1>

<div>
    <form action="#" method="get">
        <label for="searched_tag">
            Recherche par tag : <br>
        </label>
        <span class="search_span">
                <input class="search_input" type="text" name="searched_tag" id="searched_tag"
                       value="<?php if (isset($_GET["searched_tag"])) echo $_GET["searched_tag"] ?>">
                <button class="search_button" type="submit">
                <img class="search_img" src="public/assets/search.svg" alt="rechercher">

                </button>
            </span>
    </form>
</div>
<!-- ce script sera utilsé plus bas, il doit être chargé avant -->
<script src="public/js/reactions.js"></script>

<div id="messages">
    <?php
    include 'view/templateMessageList.php'
    ?>

</div>
<p id="loading_indicator">chargement ...</p>
<script src="public/js/messages.js"></script>

<?php
$content = ob_get_clean();
require('template.php');
?>
