<?php ob_start(); ?>
    <div class="form_box">

        <h2 id="send_message">
            Envoyer un message</h2>
        <form action="#" method="post" enctype="multipart/form-data">
            <label>
                Contenu
                <br/>
                <textarea name="text" class="input_txt" placeholder="50 caractères max"></textarea>
            </label>
            <br/>
            <label>
                Insérer des images <br/>
                <input type="file" accept="image/*,.jpg,.jpeg,.gif,.png,.bmp" name="images[]" multiple>

            </label>
            <br/>
            <input type="submit" name="send_message" class="button submit" value="Envoyer">
        </form>

        <h2 id="website_settings">options du site</h2>
        <form action="#" method="post">

            <label>
                Love min : <input type="number" name="min_love" min="1" class="input_txt" value="<?= $min_love ?>">
            </label>
            <br/>
            <label>
                Love max : <input type="number" name="max_love" min="1" class="input_txt" value="<?= $max_love ?>">
            </label> <br/>
            <label>
                Nombre de messages à charger : <input type="number" name="nb_load" min="1" class="input_txt"
                                                      value="<?= $nb_load ?>">
            </label> <br/>

            <input type="submit" name="update_settings" class="button submit" value="Mettre à jour">
        </form>

        <h2>Liste des utilisateurs : </h2>
        <ul id="user_list">
            <?php foreach ($users as $user): ?>
                <li><a href="index.php?action=profile&user_id=<?= $user["user_id"] ?>"><?= $user["username"] ?></a>
                    <?php if ($user["type"] != "admin") : ?>
                        <a class="danger"
                           href="index.php?action=admin&mode=delete&user_id=<?= $user["user_id"] ?>">Delete</a>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <h2>Liste des donateurs :</h2>
        <p>
            <?php

            $file_name = "liste_donateurs.txt";
            if (file_exists($file_name)) {
                $file = fopen($file_name, "r");
                if (filesize($file_name) > 0) {

                    $file_content = fread($file, filesize($file_name));
                    fclose($file);
                    echo str_replace("\n", "<br/>", $file_content);
                }
            }
            ?>
        </p>
    </div>

<?php
$content = ob_get_clean();
require('template.php');
?>