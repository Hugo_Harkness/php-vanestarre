<?php ob_start(); ?>

<div class="infos form_box">


    <?php if (isset($_SESSION["Connection"]->user_id) && ($profile->get_user_id() == $_SESSION["Connection"]->user_id || $_SESSION["Connection"]->type == "admin")): ?>

        <form action='#' method='post'>
            <h2>Informations</h2>

            <label> Nom d'utilisateur<input class="input_txt" name='username' type='text'
                                            value='<?= $profile->get_username() ?>'>
            </label>

            <label> E-mail<input class="input_txt" name='email' type='text' value='<?= $profile->get_email() ?>'>
            </label>
            <div>
                Type d’utilisateur
            </div>
            <div class="input_txt">
                <?= $profile->get_type() ?>
            </div>

            <input type='submit' name='update_infos' value='Mettre à jour' class="button submit">
        </form>
        <?php if ($profile->get_user_id() == $_SESSION["Connection"]->user_id): ?>
            <form action='#' method='post'>
                <h2>Changer de mot de passe</h2>

                <label> Ancien mot de passe<input class="input_txt" type='password' name='password'> </label>
                <label> Nouveau mot de passe<input class="input_txt" type='password' name='new_password'> </label>
                <label> Confirmation du nouveau mot de passe : <input class="input_txt" type='password'
                                                                      name='confirm_new_password'>
                </label>

                <input type='submit' name='update_infos' value='changer le mot de passe' class="button submit">
            </form>
        <?php endif; ?>

    <?php else: ?>

        <div>
            Nom d'utilisateur
        </div>
        <div class="input_txt">
            <?= $profile->get_username() ?>
        </div>

        <div>email
        </div>
        <div class="input_txt">
            <?= $profile->get_email() ?>
        </div>
        <div>
            Type d’utilisateur
        </div>
        <div class="input_txt">
            <?= $profile->get_type() ?>
        </div>


    <?php endif; ?>

</div>


<?php
$content = ob_get_clean();
require('template.php');
?>
