<?php ob_start(); ?>
<div class="form_box">

    <form method="post" action="#">
        <label for="username">Identifiant:</br></label>
        <input type="text" name="username" id="username" class="input_txt"
               value="<?php if (isset($_POST['username'])) {
                   echo $_POST['username'];
               } ?>" required/><br/>
        <label for="email">E-mail:</br></label>
        <input type="email" name="email" id="email" class="input_txt"
               value="<?php if (isset($_POST['email'])) {
                   echo $_POST['email'];
               } ?>" required/><br/>
        <label for="password">Password:</br></label>
        <input type="password" name="password" id="password" class="input_txt"
               required/><br/>
        <label for="password2">Confirm password:</br></label>
        <input type="password" name="password2" id="password2" class="input_txt"
               required/><br/>
        <input type="submit" name="signup" id="submit" value="s’inscrire" class="button submit"/>
    </form>
</div>
<?php
$content = ob_get_clean();
require('template.php');
?>

