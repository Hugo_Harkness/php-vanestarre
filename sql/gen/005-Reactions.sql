create or replace table Reactions
(
    user_id    int         not null,
    message_id int         not null,
    reaction   varchar(20) not null,
    primary key (user_id, message_id),
    constraint message_reaction
        foreign key (message_id) references Messages (message_id),
    constraint user_reaction
        foreign key (user_id) references Users (user_id)
);