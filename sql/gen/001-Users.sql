create or replace table Users
(
    user_id         int auto_increment
        primary key,
    username        varchar(255) not null,
    email           varchar(255)
                                  not null,
    passwd          varchar(255)  not null,
    type            varchar(255)  not null,
    pass_reset_code varchar(255)  null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);

