create or replace table Messages
(
    message_id    int auto_increment
        primary key,
    text          varchar(50) not null,
    image_present tinyint(1)  not null,
    nb_love_max   int         not null
);

