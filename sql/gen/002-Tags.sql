create or replace table Tags
(
	tag_id int auto_increment
		primary key,
	message_id int not null,
	tag varchar(255) not null,
	constraint message_tag
		foreign key (message_id) references Messages (message_id)
);

create or replace index message_id
	on Tags (message_id);

