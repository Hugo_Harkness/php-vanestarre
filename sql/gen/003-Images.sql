create or replace table Images
(
	image_id int auto_increment
		primary key,
	message_id int not null,
	path varchar(255) not null,
	constraint message_image
		foreign key (message_id) references Messages (message_id)
);

